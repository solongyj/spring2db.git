package com.solongyj.demo.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.solongyj.demo.entity.Product;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gaoyj
 * @since 2022-03-28
 */
public interface ProductService extends IService<Product> {

}
