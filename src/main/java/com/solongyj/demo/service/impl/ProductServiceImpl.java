package com.solongyj.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.solongyj.demo.entity.Product;
import com.solongyj.demo.mapper.db2.ProductMapper;
import com.solongyj.demo.service.ProductService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gaoyj
 * @since 2022-03-28
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {

}
