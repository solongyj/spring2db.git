package com.solongyj.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.solongyj.demo.entity.Product;
import com.solongyj.demo.entity.User;
import com.solongyj.demo.mapper.db1.UserMapper;
import com.solongyj.demo.mapper.db2.ProductMapper;
import com.solongyj.demo.service.UserService;
import com.solongyj.demo.utils.RandomUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author gaoyj
 * @since 2022-03-28
 */
@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProductMapper productMapper;

    @Override
    public void addUser(User user) {
        log.info("db1数据开始插入");
        int userRow = userMapper.insert(user);
        //这里在插入user成功后进行db2中的product的插入,这些数据全都随机生成
        if (userRow != 0) {
            log.info("db1数据插入成功");
            Product product = new Product();
            product.setPhone(RandomUtil.getPhone());
            product.setAddress(RandomUtil.getAddress());
            product.setEmail(RandomUtil.getEmail(6, 9));
            product.setBirth(new Date());
            log.info("db2数据开始插入");
            int prodRow = productMapper.insert(product);
            if (prodRow != 0) {
                log.info("db1和db2数据插入成功");
            }

        }
    }
}
