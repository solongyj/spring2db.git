package com.solongyj.demo.mapper.db2;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.solongyj.demo.entity.Product;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gaoyj
 * @since 2022-03-28
 */
public interface ProductMapper extends BaseMapper<Product> {

}
