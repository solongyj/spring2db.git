package com.solongyj.demo.mapper.db1;

import com.solongyj.demo.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gaoyj
 * @since 2022-03-28
 */

public interface UserMapper extends BaseMapper<User> {

}
